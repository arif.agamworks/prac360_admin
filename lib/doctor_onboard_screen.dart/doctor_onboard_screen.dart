import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:prac360_admin/doctor_onboard_screen.dart/doctor_onboard_controller.dart';

class DoctorOnboardScreen extends GetView<DoctorOnboardController> {
  const DoctorOnboardScreen({super.key});

  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            "Doctor OnBoard",
            style: GoogleFonts.roboto(
                fontSize: 20, fontWeight: FontWeight.bold, color: Colors.black),
          ),
          backgroundColor: Colors.white,
          elevation: 0,
        ),
        body: Padding(
          padding: const EdgeInsets.all(20),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CommonDoctorInputfields(
                  textCont: controller.doctorNameC,
                  titleText: "Doctor Name",
                ),
                CommonDoctorInputfields(
                  titleText: "Doctor age",
                  textCont: controller.doctorAgeC,
                ),
                CommonDoctorInputfields(
                  titleText: "Doctor gender",
                  textCont: controller.doctorGenderC,
                ),
                CommonDoctorInputfields(
                  textCont: controller.doctorEmailC,
                  titleText: "Doctor email",
                ),
                CommonDoctorInputfields(
                  titleText: "Doctor Contact Number",
                  textCont: controller.doctorContactC,
                ),
                CommonDoctorInputfields(
                  titleText: "Doctor Qualification",
                  textCont: controller.doctorQualificationC,
                ),
                CommonDoctorInputfields(
                  titleText: "Doctor Specialities",
                  textCont: controller.doctorQualificationC,
                ),
                CommonDoctorInputfields(
                  titleText: "Doctor YearOfExperience",
                  textCont: controller.doctorYearExpC,
                ),
                CommonDoctorInputfields(
                  titleText: "Doctor BookingPerSlot",
                  textCont: controller.doctorBookingPerSlotC,
                ),
                CommonDoctorInputfields(
                  titleText: "Doctor Description",
                  textCont: controller.doctorDescribtionC,
                ),
                CommonDoctorInputfields(
                  titleText: "Doctor Fee",
                  textCont: controller.doctorFeeC,
                ),
                CommonDoctorInputfields(
                  titleText: "Doctor Password",
                  textCont: controller.doctorPwdC,
                ),
                SizedBox(
                  height: h * .01,
                ),
                Text(
                  "Doctor logo",
                  style: GoogleFonts.dmSans(
                      fontWeight: FontWeight.w600, color: Colors.black),
                ),
                SizedBox(
                  height: h * .01,
                ),
                Row(
                  children: [
                    CommonImagePicker(
                      controller: controller,
                      imageLogoLabel: "Doctor logo",
                    ),
                    const Spacer(),
                  ],
                ),
                Center(
                  child: ElevatedButton(
                    onPressed: () {},
                    child: const Text("Add Doctor"),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class CommonImagePicker extends StatelessWidget {
  const CommonImagePicker({
    super.key,
    required this.controller,
    required this.imageLogoLabel,
  });

  final DoctorOnboardController controller;
  final String imageLogoLabel;

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Stack(
        children: [
          controller.doctorProfilePic.isEmpty
              ? const CircleAvatar(
                  maxRadius: 45,
                  backgroundColor: Color.fromARGB(255, 225, 221, 221),
                  child: Icon(
                    Icons.person,
                    color: Colors.grey,
                    size: 50,
                  ),
                )
              : InkWell(
                  onTap: () {},
                  child: CachedNetworkImage(
                    fit: BoxFit.cover,
                    imageBuilder: (context, imageProvider) => CircleAvatar(
                      backgroundImage: imageProvider,
                      radius: 70,
                    ),
                    useOldImageOnUrlChange: true,
                    placeholder: (context, url) => const CircleAvatar(
                      backgroundColor: Colors.transparent,
                      maxRadius: 70,
                      child: CircularProgressIndicator(),
                    ),
                    errorWidget: (context, url, error) =>
                        const Icon(Icons.error),
                    imageUrl: controller.doctorProfilePic.value,
                  ),
                ),
          Positioned(
            bottom: 5,
            left: 60,
            child: InkWell(
              onTap: () {
                chooseCameraOrGalleryPopup(context);
              },
              child: Container(
                width: 20,
                height: 20,
                decoration: const BoxDecoration(
                  color: Colors.blue,
                  shape: BoxShape.rectangle,
                ),
                child: const Icon(
                  Icons.edit,
                  color: Colors.white,
                  size: 15,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

chooseCameraOrGalleryPopup(context) {
  showModalBottomSheet(
    context: context,
    builder: (context) {
      return SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 40,
            ),
            const Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: 20,
                ),
                child: Text("Choose Source")),
            const SizedBox(
              height: 20,
            ),
            Row(
              children: [
                const SizedBox(
                  width: 20,
                ),
                chooseProfilePhotoPopup(
                    context,
                    ImageSource.camera,
                    const Icon(
                      Icons.camera_alt,
                      color: Colors.blue,
                    ),
                    "Camera"),
                const SizedBox(
                  width: 20,
                ),
                chooseProfilePhotoPopup(
                    context,
                    ImageSource.gallery,
                    const Icon(
                      Icons.photo,
                      color: Colors.blue,
                    ),
                    "Gallery")
              ],
            ),
            const SizedBox(
              height: 30,
            ),
          ],
        ),
      );
    },
  );
}

InkWell chooseProfilePhotoPopup(
    BuildContext context, ImageSource imageSource, Icon icon, String text) {
  final cont = Get.find<DoctorOnboardController>();
  return InkWell(
    onTap: () async {
      final image = await ImagePicker().pickImage(source: imageSource);

      if (image == null) {
        return;
      }
      cont.doctorProfilePicFile = File(image.path);
      print("ImagePath ${cont.doctorProfilePicFile}");
      cont.addUploadImage();
    },
    child: Column(
      children: [
        Container(
            width: 40,
            height: 40,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(
                color: const Color.fromARGB(255, 213, 201, 201),
                // width: 2.0,
              ),
            ),
            child: icon),
        const SizedBox(
          height: 10,
        ),
        Text(
          text,
          style: GoogleFonts.dmSans(
              fontWeight: FontWeight.w400, color: Colors.grey),
        )
      ],
    ),
  );
}

class CommonDoctorInputfields extends StatelessWidget {
  const CommonDoctorInputfields({
    super.key,
    required this.titleText,
    required this.textCont,
  });
  final String titleText;
  final TextEditingController textCont;

  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: h * .02,
        ),
        Text(
          titleText,
          style: GoogleFonts.dmSans(
              fontWeight: FontWeight.w600, color: Colors.black),
        ),
        SizedBox(
          height: h * .01,
        ),
        TextFormField(
          controller: textCont,
          onTap: () {},
          decoration: const InputDecoration(
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.blue)),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.blue)),
              counterText: "",
              isDense: false,
              border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.blue))),
        ),
      ],
    );
  }
}
