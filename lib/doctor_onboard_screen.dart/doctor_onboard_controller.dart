import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:prac360_admin/hospital_onboard_screen/hospital_onboard_controller.dart';
import 'package:prac360_admin/library/data_provider.dart';

class DoctorOnboardController extends GetxController {
  late File doctorProfilePicFile;
  var doctorProfilePic = "".obs;
  TextEditingController doctorNameC = TextEditingController();
  TextEditingController doctorAgeC = TextEditingController();
  TextEditingController doctorGenderC = TextEditingController();
  TextEditingController doctorEmailC = TextEditingController();
  TextEditingController doctorContactC = TextEditingController();
  TextEditingController doctorQualificationC = TextEditingController();
  TextEditingController doctorSpecialitiesC = TextEditingController();
  TextEditingController doctorYearExpC = TextEditingController();
  TextEditingController doctorBookingPerSlotC = TextEditingController();
  TextEditingController doctorDescribtionC = TextEditingController();
  TextEditingController doctorFeeC = TextEditingController();
  TextEditingController doctorPwdC = TextEditingController();
  Map<String, dynamic> doctorOnboardData = {};
  // String doctorPrefix = "";

  addUploadImage() async {
    String fileName = DateTime.now().millisecondsSinceEpoch.toString();
    Reference firebaseStorageRef =
        FirebaseStorage.instance.ref().child('images/$fileName');
    UploadTask uploadTask = firebaseStorageRef.putFile(doctorProfilePicFile);

    TaskSnapshot taskSnapshot = await uploadTask.whenComplete(() => null);
    String downloadURL = await taskSnapshot.ref.getDownloadURL();

    print("Image uploaded to Firebase Storage: $downloadURL");
    doctorProfilePic.value = downloadURL;
  }

  String splitFun() {
    List<String> namePre = doctorNameC.text.split(' ');
    String res = "";
    for (var na in namePre) {
      res += na[0];
    }
    print(res);
    return res;
  }

  doctorOnBoarFun() {
    doctorOnboardData = {
      "name": doctorNameC.text,
      "age": doctorAgeC.text,
      "gender": doctorGenderC.text,
      "profilePic": doctorProfilePic.value,
      "email": doctorEmailC.text,
      "contactNo": doctorEmailC.text,
      "website": splitFun(),
      "profileDetails": {},
      "prefix": "",
      "qualification": {"major": doctorQualificationC.text},
      "specialties": [doctorSpecialitiesC.text],
      "yearsOfExp": int.parse(doctorYearExpC.text),
      "bookingPerSlot": int.parse(doctorBookingPerSlotC.text),
      "doctorDetails": {
        "primary": true,
        "hospital": Get.find<HospitalOnboardController>().hospitalDetails,
        "description": doctorDescribtionC.text,
        "other_locations": [],
      },
      "specializations": [],
      "fee": int.parse(doctorFeeC.text),
      "password": doctorPwdC.text
    };
    DataProvider().postDoctorOnBoard(
      doctorOnboardData,
      onSuccess: (response) {
        print(response);
      },
      onError: (error) {
        print(error);
      },
    );
  }
}
