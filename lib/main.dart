import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:prac360_admin/doctor_onboard_screen.dart/doctor_onboard_controller.dart';
import 'package:prac360_admin/firebase_options.dart';
import 'package:prac360_admin/hospital_onboard_screen/hospital_onboard_controller.dart';
import 'package:prac360_admin/main_controller.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  runApp(const MyApp());
  Get.put(MainController());
  Get.put(HospitalOnboardController());
  Get.put(DoctorOnboardController());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const GetMaterialApp(
      debugShowCheckedModeBanner: false,
      home: SplashScreen(),
    );
  }
}

class SplashScreen extends GetView<MainController> {
  const SplashScreen({super.key});

  @override
  Widget build(BuildContext context) {
    Future.delayed(const Duration(seconds: 2), () async {
      await controller.getToken();
    });
    return const Scaffold(
      backgroundColor: Color(0xFF2F80Ec),
      body: Stack(
        children: [
          Align(
            alignment: Alignment.center,
            child: Image(
              image: AssetImage(
                "assets/images/logo.jpeg",
              ),
            ),
          ),
          Positioned(
            bottom: 0,
            right: 0,
            left: 0,
            child: SpinKitCircle(
              color: Colors.white,
            ),
          )
        ],
      ),
    );
  }
}
