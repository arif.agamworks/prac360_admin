import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';
import 'package:prac360_admin/globals.dart' as g;
import 'package:prac360_admin/library/data_provider.dart';

class MainController extends GetxController {
  FirebaseAuth auth = FirebaseAuth.instance;
  String email = "thirunavu.c@gmail.com";
  String pwd = "abc12345";
  Logger log = Logger();

  getToken() {
    auth
        .signInWithEmailAndPassword(email: email, password: pwd)
        .then((value) async {
      print(value);
      g.doctorAuthDetails['email'] = email;
      await g.storeDoctorsAuthDetails(value);
    }).catchError((e) {
      print(e);
    });

    // AuthenticationHelper().signIn(email, pwd).then((value) async {
    //   print(value);
    //   await g.storeDoctorsAuthDetails(value);
    // });
  }
}
