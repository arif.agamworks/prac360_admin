import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:prac360_admin/doctor_onboard_screen.dart/doctor_onboard_screen.dart';
import 'package:prac360_admin/hospital_onboard_screen/hospital_onboard_controller.dart';

class HospitalOnBoardScreen extends GetView<HospitalOnboardController> {
  const HospitalOnBoardScreen({super.key});

  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            "Hospital OnBoard",
            style: GoogleFonts.roboto(
                fontSize: 20, fontWeight: FontWeight.bold, color: Colors.black),
          ),
          backgroundColor: Colors.white,
          elevation: 0,
        ),
        body: Padding(
          padding: const EdgeInsets.all(20),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CommonHospitalInputfields(
                  textCont: controller.hospitalNameC,
                  titleText: "Hospital Name",
                ),
                CommonHospitalInputfields(
                  titleText: "Hospital Address",
                  textCont: controller.hospitalAddressC,
                ),
                SizedBox(
                  height: h * .02,
                ),
                Text(
                  "Choose Hospital Area",
                  style: GoogleFonts.dmSans(
                      fontWeight: FontWeight.w600, color: Colors.black),
                ),
                SizedBox(
                  height: h * .01,
                ),
                GetBuilder<HospitalOnboardController>(builder: (con) {
                  return DropdownButtonFormField2(
                      onChanged: (value) {
                        print(value);
                        con.addArea(value);
                      },
                      decoration: const InputDecoration(
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.blue)),
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.blue)),
                          counterText: "",
                          isDense: false,
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.blue))),
                      items: con.hospitalAreaList
                          .map((e) => DropdownMenuItem(
                              value: e,
                              child: Text(
                                e["name"],
                                // style: GoogleFonts.dmSans(
                                //     fontWeight: FontWeight.w600,
                                //     color: Colors.black),
                              )))
                          .toList());
                }),
                CommonHospitalInputfields(
                  titleText: "Hospital Contact Number",
                  textCont: controller.hospitalContactNumberC,
                ),
                SizedBox(
                  height: h * .01,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Hospital logo",
                      style: GoogleFonts.dmSans(
                          fontWeight: FontWeight.w600, color: Colors.black),
                    ),
                    Text(
                      "Hospital banner",
                      style: GoogleFonts.dmSans(
                          fontWeight: FontWeight.w600, color: Colors.black),
                    ),
                  ],
                ),
                SizedBox(
                  height: h * .01,
                ),
                Row(
                  children: [
                    CommonImagePicker(
                      controller: controller,
                      imageLogoLabel: "Hospital logo",
                    ),
                    const Spacer(),
                    BannerImagePicker(
                        controller: controller,
                        imageLogoLabel: "Hospital banner")
                  ],
                ),
                SizedBox(
                  height: h * .02,
                ),
                Center(
                  child: ElevatedButton(
                    onPressed: () {
                      controller.obBoardHospitalFuc();
                    },
                    child: const Text("Add Hospital"),
                  ),
                ),
                Center(
                  child: ElevatedButton(
                    onPressed: () {
                      Get.to(const DoctorOnboardScreen());
                    },
                    child: const Text("Doctor onboard page"),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class CommonImagePicker extends StatelessWidget {
  const CommonImagePicker({
    super.key,
    required this.controller,
    required this.imageLogoLabel,
  });

  final HospitalOnboardController controller;
  final String imageLogoLabel;

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Stack(
        children: [
          controller.hospitalLogoImage.isEmpty
              ? const CircleAvatar(
                  maxRadius: 45,
                  backgroundColor: Color.fromARGB(255, 225, 221, 221),
                  child: Icon(
                    Icons.person,
                    color: Colors.grey,
                    size: 50,
                  ),
                )
              : InkWell(
                  onTap: () {},
                  child: CachedNetworkImage(
                    fit: BoxFit.cover,
                    imageBuilder: (context, imageProvider) => CircleAvatar(
                      backgroundImage: imageProvider,
                      radius: 70,
                    ),
                    useOldImageOnUrlChange: true,
                    placeholder: (context, url) => const CircleAvatar(
                      backgroundColor: Colors.transparent,
                      maxRadius: 70,
                      child: CircularProgressIndicator(),
                    ),
                    errorWidget: (context, url, error) =>
                        const Icon(Icons.error),
                    imageUrl: controller.hospitalLogoImage.value,
                  ),
                ),
          Positioned(
            bottom: 5,
            left: 60,
            child: InkWell(
              onTap: () {
                controller.isHospitalBannerFlag.value = false;
                controller.isHospitalLogoFlag.value = true;
                chooseCameraOrGalleryPopup(context);
              },
              child: Container(
                width: 20,
                height: 20,
                decoration: const BoxDecoration(
                  color: Colors.blue,
                  shape: BoxShape.rectangle,
                ),
                child: const Icon(
                  Icons.edit,
                  color: Colors.white,
                  size: 15,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class BannerImagePicker extends StatelessWidget {
  const BannerImagePicker({
    super.key,
    required this.controller,
    required this.imageLogoLabel,
  });

  final HospitalOnboardController controller;
  final String imageLogoLabel;

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Stack(
        children: [
          controller.hospitalBannerImage.isEmpty
              ? const CircleAvatar(
                  maxRadius: 45,
                  backgroundColor: Color.fromARGB(255, 225, 221, 221),
                  child: Icon(
                    Icons.person,
                    color: Colors.grey,
                    size: 50,
                  ),
                )
              : InkWell(
                  onTap: () {},
                  child: CachedNetworkImage(
                    fit: BoxFit.cover,
                    imageBuilder: (context, imageProvider) => CircleAvatar(
                      backgroundImage: imageProvider,
                      radius: 70,
                    ),
                    useOldImageOnUrlChange: true,
                    placeholder: (context, url) => const CircleAvatar(
                      backgroundColor: Colors.transparent,
                      maxRadius: 70,
                      child: CircularProgressIndicator(),
                    ),
                    errorWidget: (context, url, error) =>
                        const Icon(Icons.error),
                    imageUrl: controller.hospitalBannerImage.value,
                  ),
                ),
          Positioned(
            bottom: 5,
            left: 60,
            child: InkWell(
              onTap: () {
                controller.isHospitalBannerFlag.value = true;
                controller.isHospitalLogoFlag.value = false;
                chooseCameraOrGalleryPopup(context);
              },
              child: Container(
                width: 20,
                height: 20,
                decoration: const BoxDecoration(
                  color: Colors.blue,
                  shape: BoxShape.rectangle,
                ),
                child: const Icon(
                  Icons.edit,
                  color: Colors.white,
                  size: 15,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

chooseCameraOrGalleryPopup(context) {
  showModalBottomSheet(
    context: context,
    builder: (context) {
      return SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 40,
            ),
            const Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: 20,
                ),
                child: Text("Choose Source")),
            const SizedBox(
              height: 20,
            ),
            Row(
              children: [
                const SizedBox(
                  width: 20,
                ),
                chooseProfilePhotoPopup(
                    context,
                    ImageSource.camera,
                    const Icon(
                      Icons.camera_alt,
                      color: Colors.blue,
                    ),
                    "Camera"),
                const SizedBox(
                  width: 20,
                ),
                chooseProfilePhotoPopup(
                    context,
                    ImageSource.gallery,
                    const Icon(
                      Icons.photo,
                      color: Colors.blue,
                    ),
                    "Gallery")
              ],
            ),
            const SizedBox(
              height: 30,
            ),
          ],
        ),
      );
    },
  );
}

InkWell chooseProfilePhotoPopup(
    BuildContext context, ImageSource imageSource, Icon icon, String text) {
  final cont = Get.find<HospitalOnboardController>();
  return InkWell(
    onTap: () async {
      final image = await ImagePicker().pickImage(source: imageSource);

      if (image == null) {
        return;
      }
      cont.hospitalLogoImageFile = File(image.path);
      print("ImagePath ${cont.hospitalLogoImageFile}");
      cont.addUploadImage();
    },
    child: Column(
      children: [
        Container(
            width: 40,
            height: 40,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(
                color: const Color.fromARGB(255, 213, 201, 201),
                // width: 2.0,
              ),
            ),
            child: icon),
        const SizedBox(
          height: 10,
        ),
        Text(
          text,
          style: GoogleFonts.dmSans(
              fontWeight: FontWeight.w400, color: Colors.grey),
        )
      ],
    ),
  );
}

class CommonHospitalInputfields extends StatelessWidget {
  const CommonHospitalInputfields({
    super.key,
    required this.titleText,
    required this.textCont,
  });
  final String titleText;
  final TextEditingController textCont;

  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: h * .02,
        ),
        Text(
          titleText,
          style: GoogleFonts.dmSans(
              fontWeight: FontWeight.w600, color: Colors.black),
        ),
        SizedBox(
          height: h * .01,
        ),
        TextFormField(
          controller: textCont,
          onTap: () {},
          decoration: const InputDecoration(
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.blue)),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.blue)),
              counterText: "",
              isDense: false,
              border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.blue))),
        ),
      ],
    );
  }
}
