import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:prac360_admin/library/data_provider.dart';

class HospitalOnboardController extends GetxController {
  late File hospitalLogoImageFile;
  var hospitalLogoImage = "".obs;
  var hospitalBannerImage = "".obs;
  var isHospitalLogoFlag = false.obs;
  var isHospitalBannerFlag = false.obs;
  TextEditingController hospitalNameC = TextEditingController();
  TextEditingController hospitalAddressC = TextEditingController();
  TextEditingController hospitalContactNumberC = TextEditingController();
  var hospitalAreaId = "".obs;
  Map hospitalImages = {};
  Map<String, dynamic> hospitalOnboardData = {};
  var hospitalAreaList = [].obs;
  var hospitalAreaNameList = [].obs;
  Map hospitalDetails = {};

  addUploadImage() async {
    String fileName = DateTime.now().millisecondsSinceEpoch.toString();
    Reference firebaseStorageRef =
        FirebaseStorage.instance.ref().child('images/$fileName');
    UploadTask uploadTask = firebaseStorageRef.putFile(hospitalLogoImageFile);

    TaskSnapshot taskSnapshot = await uploadTask.whenComplete(() => null);
    String downloadURL = await taskSnapshot.ref.getDownloadURL();

    print("Image uploaded to Firebase Storage: $downloadURL");
    if (isHospitalLogoFlag.value) {
      hospitalLogoImage.value = downloadURL;
    } else if (isHospitalBannerFlag.value) {
      hospitalBannerImage.value = downloadURL;
    }
  }

  addArea(value) {
    hospitalAreaId.value = value["id"];
    print(hospitalAreaId.value);
    update();
  }

  getAllAreas() {
    DataProvider().getAllHospitalArea(
      onSuccess: (response) {
        print(response);
        for (var hospitalList in response) {
          hospitalAreaList.add(hospitalList);
          hospitalAreaNameList.add(hospitalList["name"]);
        }
        print(response);
        print(hospitalAreaList);
        print(hospitalAreaNameList);
      },
      onError: (error) {
        print(error);
      },
    );
  }

  obBoardHospitalFuc() {
    hospitalImages = {
      "logo": hospitalLogoImage.value,
      "banner": hospitalBannerImage.value
    };
    hospitalOnboardData = {
      "name": hospitalNameC.text,
      "address": hospitalAddressC.text,
      "areaId": hospitalAreaId.value,
      "contactNo": hospitalContactNumberC.text,
      "website": hospitalImages.toString(),
      "details": {}
    };
    print(hospitalImages);
    print(hospitalOnboardData);
    DataProvider().postHospitalOnBoard(
      hospitalOnboardData,
      onSuccess: (response) {
        print(response);
        print(
          response["data"]["code"],
        );

        hospitalDetails = {
          "code": response["data"]["code"],
          "name": response["data"]["name"],
          "areaId": response["data"]["areaId"],
          "address": response["data"]["address"],
          "contactNo": response["data"]["contactNo"]
        };
        print(hospitalDetails);
      },
      onError: (error) {
        print(error);
      },
    );
  }
}
