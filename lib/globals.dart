import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:get/get.dart';
import 'package:prac360_admin/hospital_onboard_screen/hospital_onboard_controller.dart';
import 'package:prac360_admin/hospital_onboard_screen/hospital_onboard_screen.dart';

Map<String, String> doctorAuthDetails = {'jwt': ""};
FirebaseRemoteConfig remoteConfig = FirebaseRemoteConfig.instance;
storeDoctorsAuthDetails(user) async {
  if (user != null) {
    FirebaseAuth.instance.currentUser!.getIdToken().then((jwt) {
      print(jwt);
      doctorAuthDetails['jwt'] = jwt!;
    });
    print(doctorAuthDetails);
    Future.delayed(Duration(seconds: 3), () {
      Get.find<HospitalOnboardController>().getAllAreas();
      Get.off(const HospitalOnBoardScreen());
    });
  }
}
