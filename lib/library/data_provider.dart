import 'package:prac360_admin/library/api_request.dart';

class DataProvider {
  void postHospitalOnBoard(
    hospitalData, {
    Function(dynamic response)? onSuccess,
    Function(dynamic error)? onError,
  }) {
    ApiRequest(url: "onboard/hospital", query: {}, data: hospitalData).post(
      onSuccess: (data) {
        onSuccess!(data);
      },
      onError: (error) {
        onError!(error);
      },
    );
  }

  void getAllHospitalArea({
    Function(dynamic response)? onSuccess,
    Function(dynamic error)? onError,
  }) {
    ApiRequest(url: "area", query: {}, data: {}).get(
      onSuccess: (data) {
        onSuccess!(data);
      },
      onError: (error) {
        onError!(error);
      },
    );
  }

  void postDoctorOnBoard(
    doctorData, {
    Function(dynamic response)? onSuccess,
    Function(dynamic error)? onError,
  }) {
    ApiRequest(url: "onboard/doctor", query: {}, data: doctorData).post(
      onSuccess: (data) {
        onSuccess!(data);
      },
      onError: (error) {
        onError!(error);
      },
    );
  }
}
