import 'package:dio/dio.dart';
import 'package:prac360_admin/globals.dart' as g;
import 'package:dio_logger/dio_logger.dart';

class BaseDio {
  // static const devServerUrl = "https://prac360-api.up.railway.app/";
  static const devServerUrl = "https://api.dev.prac360.com/";
  static const prodServerUrl = 'https://api.prod.prac360.com/';
  String baseUrl = devServerUrl;
  // const String.fromEnvironment('base_url', defaultValue: prodServerUrl);
  Dio? _dio;
  final Map<String, String> _headers = {
    'Authorization': 'Bearer ${g.doctorAuthDetails['jwt']!}',
    'Content-Type': 'application/json'
  };
  //Private constructor...
  // BaseDio._() {
  //   _dio = Dio(
  //     BaseOptions(
  //       headers: {
  //         'Authorization': 'Bearer ${g.doctorAuthDetails['jwt']!}',
  //         'Content-Type': 'application/json'
  //       },
  //       baseUrl: devServerUrl,
  //       connectTimeout: 30000,
  //       receiveTimeout: 30000,
  //       followRedirects: false,
  //       validateStatus: (status) => true,
  //     ),
  //   );
  //   dio!.interceptors.add(dioLoggerInterceptor);
  // }

  getDio() {
    _dio ??= Dio(BaseOptions(
      headers: _headers,
      baseUrl: BaseDio.instance.baseUrl,
      connectTimeout: 30000,
      receiveTimeout: 30000,
      followRedirects: false,
      validateStatus: (status) => true,
    ));
    _dio!.interceptors.add(dioLoggerInterceptor);
    return _dio;
  }

  static final BaseDio instance = BaseDio();
}

class ApiRequest {
  final String url;
  final Map<String, dynamic> query;
  final dynamic data;

  ApiRequest({required this.url, required this.query, required this.data});

  void get({
    Function()? beforeSend,
    Function(dynamic data)? onSuccess,
    Function(dynamic error)? onError,
  }) {
    BaseDio.instance.getDio().get(url, queryParameters: query).then((res) {
      if (res is Map &&
          res.containsKey('statusCode') &&
          ![200, 201].contains(res['statusCode'])) {
        if (onError != null) onError(res['message']);
      } else {
        if (onSuccess != null) {
          onSuccess(res.data);
        }
      }
    }).catchError((error) {
      if (onError != null) onError(error);
    });
  }

  Future<void> post({
    Function()? beforeSend,
    Function(dynamic data)? onSuccess,
    Function(dynamic error)? onError,
  }) async {
    BaseDio.instance
        .getDio()
        .post(url, queryParameters: query, data: data)
        .then((res) {
      if (res is Map &&
          res.containsKey('statusCode') &&
          ![200, 201].contains(res['statusCode'])) {
        if (onError != null) onError(res['message']);
      } else {
        if (onSuccess != null) {
          onSuccess(res.data);
        }
      }
    }).catchError((error) {
      if (onError != null) onError(error);
    });
  }

  Future<void> patch({
    Function()? beforeSend,
    Function(dynamic data)? onSuccess,
    Function(dynamic error)? onError,
  }) async {
    BaseDio.instance
        .getDio()
        .patch(url, queryParameters: query, data: data)
        .then((res) {
      if (res is Map &&
          res.containsKey('statusCode') &&
          ![200, 201].contains(res['statusCode'])) {
        if (onError != null) onError(res['message']);
      } else {
        if (onSuccess != null) {
          onSuccess(res.data);
        }
      }
    }).catchError((error) {
      if (onError != null) onError(error);
    });
  }

  void getApiWithoutToken(
    header, {
    Function()? beforeSend,
    Function(dynamic data)? onSuccess,
    Function(dynamic error)? onError,
  }) {
    dynamic dio = Dio(BaseOptions(
      headers: header,
      baseUrl: BaseDio.instance.baseUrl,
      connectTimeout: 30000,
      receiveTimeout: 30000,
      followRedirects: false,
      validateStatus: (status) => true,
    ));

    dio
        .get(
      url,
      queryParameters: query,
    )
        .then((res) {
      if (res is Map &&
          res.containsKey('statusCode') &&
          ![200, 201].contains(res['statusCode'])) {
        if (onError != null) onError(res['message']);
      } else {
        if (onSuccess != null) {
          onSuccess(res.data);
        }
      }
    }).catchError((error) {
      if (onError != null) onError(error);
    });
    dio.interceptors.add(dioLoggerInterceptor);
  }

  postApiWithoutToken(
    header, {
    Function()? beforeSend,
    Function(dynamic data)? onSuccess,
    Function(dynamic error)? onError,
  }) {
    dynamic dio = Dio(BaseOptions(
      headers: header,
      baseUrl: BaseDio.instance.baseUrl,
      connectTimeout: 30000,
      receiveTimeout: 30000,
      followRedirects: false,
      validateStatus: (status) => true,
    ));

    dio.post(url, queryParameters: query, data: data).then((res) {
      if (res is Map &&
          res.containsKey('statusCode') &&
          ![200, 201].contains(res['statusCode'])) {
        if (onError != null) onError(res['message']);
      } else {
        if (onSuccess != null) {
          onSuccess(res.data);
        }
      }
    }).catchError((error) {
      if (onError != null) onError(error);
    });
    dio.interceptors.add(dioLoggerInterceptor);
  }

  void getAppLabelWithOutToken({
    Function()? beforeSend,
    Function(dynamic data)? onSuccess,
    Function(dynamic error)? onError,
  }) {
    dynamic localDio = Dio();
    localDio.get(url, queryParameters: query).then((res) {
      if (res is Map &&
          res.containsKey('statusCode') &&
          ![200, 201].contains(res['statusCode'])) {
        if (onError != null) onError(res['message']);
      } else {
        if (onSuccess != null) {
          onSuccess(res.data);
        }
      }
    }).catchError((error) {
      if (onError != null) onError(error);
    });
  }
}
